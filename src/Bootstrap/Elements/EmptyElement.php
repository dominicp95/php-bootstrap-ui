<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\Elements;

class EmptyElement {
    private $_html = "";
    
    public function __construct (
        $html
    ){
        $this->setHtml($html);
    }
    
    public function setHtml($html){
        $this->_html = $html;
        return $this;
    }
    public function appendHtml($html){
        return $this->_html .= $html;
    }
    public function getHtml(){
        return $this->_html;
    }
    
    public function render(){
        echo $this->renderBuffer();
    }
    public function renderBuffer(){
        $res = $this->getHTML();
        return $res;
    }
    
    public function __toString(){
        return $this->renderBuffer();
    }
}
