<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\Elements;

class ElementGroup {
    private $_tag = "";
    private $_attributes = array();
    private $_children = array();
    
    public function __construct(
        $children,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        
        $this->_children = $children;
        if(!is_null($attributes)){ $this->_attributes = $attributes; }
        
        if(!is_null($class)){
            foreach($class as $c){
                $this->appendAttribute('class', $c);
            }
        }
        
        if(!is_null($style)){
            foreach($style as $key=>$value){
                $this->appendAttribute('style', $key.': '.$value.';');
            }
        }
    }
    
    public function setTag($tag){
        $this->_tag = $tag;
        return $this;
    }
    public function getTag(){
        return $this->_tag;
    }
    
    public function addAttribute($attr, $value){
        $this->_attributes[$attr] = trim( $value );
        return $this;
    }
    public function removeAttribute($atr){
        unset( $this->_attributes[$attr] );
        return $this;
    }
    public function appendAttribute($attr, $value){
        if(!isset($this->_attributes[$attr])) { return $this->addAttribute($attr, $value); }
        
        $this->_attributes[$attr] = trim( $this->_attributes[$attr].' '.$value );
        return $this;
    }
    
    protected function renderAttr(){
        $r = '';
        foreach($this->_attributes as $k=>$v){
            $r .= $k.'="'.$v.'" ';
        }
        return trim($r);
    }
    
    public function render(){
        echo $this->renderBuffer();
    }
    public function renderBuffer(){
        ob_start();
        
        if($this->getTag()!==""){ echo '<'.trim($this->_tag.' '.$this->renderAttr()).'> '; }
        foreach($this->_children as $c){           
            $this->renderChild($c); 
            echo ' ';
        }
        if($this->getTag()!==""){ echo ' </'.$this->getTag().'>'; }
        
        return ob_get_clean();
    }
    
    public function __toString(){
        return $this->renderBuffer();
    }
    
    private function renderChild($children){
        if(is_array($children)){
            foreach($children as $c){           
                $this->renderChild($c);
            }
        }else{ if(method_exists($children, 'render')){ $children->render(); echo ' '; } }
    }
}
