<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\Elements;

class LogicElement {
    private $_logic = null;
    private $_args = null;
    
    public function __construct (
        $logic,
        $args = null
    ){
        $this->setLogic($logic);
        $this->setArgs($args);
    }
    
    public function getLogic(){
        return $this->_logic;
    }
    public function setLogic($logic){
        $this->_logic = $logic;
    }
    
    public function getArgs(){
        return $this->_args;
    }
    public function setArgs($args){
        $this->_args = $args;
    }
    
    public function render(){
        if(is_array($this->getArgs())){
            echo call_user_func_array($this->getLogic(), $this->getArgs());
        }else{
            echo call_user_func($this->getLogic());
        }
    }
    public function renderBuffer(){
        ob_start();
        $this->render();
        
        return ob_get_clean();
    }
    
    public function __toString(){
        return $this->renderBuffer();
    }
}
