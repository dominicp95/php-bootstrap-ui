<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\Elements;

class SingleElement {
    private $_tag = "";
    private $_html = "";
    private $_selfClose = false;
    private $_attributes = array();
    private $_openTag = false;
    
    public function __construct (
        $html,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        $this->_html = $html;
        
        if(!is_null($attributes)){ $this->_attributes = $attributes; }
        
        if(!is_null($class)){
            foreach($class as $c){
                $this->appendAttribute('class', $c);
            }
        }
        
        if(!is_null($style)){
            foreach($style as $key=>$value){
                $this->appendAttribute('style', $key.': '.$value.';');
            }
        }
    }
    
    public function setTag($tag){
        $this->_tag = $tag;
        return $this;
    }
    public function getTag(){
        return $this->_tag;
    }
    
    public function setHtml($html){
        $this->_html = $html;
        return $this;
    }
    public function appendHtml($html){
        return $this->_html .= $html;
    }
    public function getHtml(){
        return $this->_html;
    }
    
    public function selfClose($selfClose=null){
        if(is_bool($selfClose)){
            $this->_selfClose = $selfClose;
            return $this;
        }else{
            return $this->_selfClose;
        }
    }
    public function openTag($openTag=null){
        if(is_bool($openTag)){
            $this->_openTag = $openTag;
            return $this;
        }else{
            return $this->_openTag;
        }
    }
    
    public function addAttribute($attr, $value){
        $this->_attributes[$attr] = trim( $value );
        return $this;
    }
    public function removeAttribute($atr){
        unset( $this->_attributes[$attr] );
        return $this;
    }
    public function appendAttribute($attr, $value){
        if(!isset($this->_attributes[$attr])) { return $this->addAttribute($attr, $value); }
        
        $this->_attributes[$attr] = trim( $this->_attributes[$attr].' '.$value );
        return $this;
    }
    
    protected function renderAttr(){
        $r = '';
        foreach($this->_attributes as $k=>$v){
            
            $r .= $k.'="'.$v.'" ';
        }
        return trim($r);
    }
    
    public function render(){
        echo $this->renderBuffer();
    }
    public function renderBuffer(){
        $res = '<'.trim($this->_tag.' '.$this->renderAttr()). (($this->selfClose()===false)? '> ' : '');
        if($this->selfClose()===false){ $res .= $this->getHTML(); }
        
        if($this->selfClose() === true){
            $res .= ' />';
        }else{
            $res .= ' </'.$this->getTag().'>';
        }
        return $res;
    }
    
    public function __toString(){
        return $this->renderBuffer();
    }
}
