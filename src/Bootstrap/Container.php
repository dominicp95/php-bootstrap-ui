<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap;

class Container extends Elements\ElementGroup{
    const FLUID = "container-fluid";
    const FIXED = "container";
    
    public function __construct(
        $type=null, 
        $children=array(),
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct($children, $class, $style, $attributes);
        
        $this->setTag('div');
        if(!is_null($type)){ $this->appendAttribute('class', $type); }
    }
}
