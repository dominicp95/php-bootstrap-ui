<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap;
use Bootstrap\Elements\ElementGroup;

class Html extends ElementGroup{
    
    public function __construct(
        $lang,
        $children=array(),
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct($children, $class, $style, $attributes);
        $this->setTag('html');
        $this->addAttribute('lang', $lang);
    }
    
    public function renderBuffer(){
        return '<!DOCTYPE html>'.parent::renderBuffer();
    }
    
    
}
