<?php
namespace Bootstrap;

abstract class UI
{
    const TEXT_LEFT = 'text-left';
    const TEXT_RIGHT = 'text-right';
    const TEXT_CENTER = 'text-center';
    const TEXT_JUSTIFY = 'text-justify';
    const TEXT_NOWRAP = 'text-nowrap';
    
    const TEXT_UPPERCASE = 'text-uppercase';
    const TEXT_LOWERCASE = 'text-lowercase';
    const TEXT_CAPITALIZE = 'text-capitalize';
    // etc.
}
