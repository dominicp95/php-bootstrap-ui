<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\Html;
use Bootstrap\Elements\SingleElement;

class InlineStyle extends SingleElement{
   
    public function __construct(
        $style,
        $type=null
    ){
        parent::__construct( $style, array(), array(), array() );
        
        if(!is_null($type)){ $this->addAttribute('type', $type); }
        $this->setTag('style');
    }
}
