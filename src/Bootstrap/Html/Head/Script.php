<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\Html\Head;
use Bootstrap\Elements\SingleElement;

class Script extends SingleElement{
   
    public function __construct(
        $href,
        $type=null
    ){
        parent::__construct('', array(), array(), array());
        
        $this->addAttribute('src',$href);
        if(!is_null($type)){ $this->addAttribute('type',$type); }
        
        $this->setTag('script');
    }
}
