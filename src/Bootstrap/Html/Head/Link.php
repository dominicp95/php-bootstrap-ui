<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\Html\Head;
use Bootstrap\Elements\SingleElement;

class Link extends SingleElement{
   
    public function __construct(
        $href,
        $rel="stylesheet",
        $attributes=[]
    ){
        parent::__construct('', [], [], $attributes);
        
        $this->addAttribute('href',$href);
        $this->addAttribute('rel',$rel);
        
        $this->setTag('link');
        $this->selfClose(true);
    }
}
