<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\Html\Head;
use Bootstrap\Elements\SingleElement;

class Meta extends SingleElement{
   
    public function __construct(
        $name,
        $content
    ){
        parent::__construct('', array(), array(), array());
        
        $this->addAttribute('name',$name);
        $this->addAttribute('content',$content);
        
        $this->setTag('meta');
        $this->selfClose(true);
    }
}
