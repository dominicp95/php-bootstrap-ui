<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Elements\SingleElement;

class FAIcon extends SingleElement{
    
    const SIZE_1X = "";
    const SIZE_2X = "fa-2x";
    const SIZE_3X = "fa-3x";
    const SIZE_4X = "fa-4x";
    const SIZE_5X = "fa-5x";
    const SIZE_LARGE = "fa-lg";
    
    const WIDTH_DEFAULT = "";
    const WIDTH_FIXED = "fa-fw";
    
    public function __construct(
        $icon,
        $size=FAIcon::SIZE_1X,
        $width=FAIcon::WIDTH_DEFAULT,
            
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
         parent::__construct('', $class, $style, $attributes);
        
        $this->setTag('i');
        $this->appendAttribute("class", "fa");
        $this->appendAttribute("class", trim("fa-$icon $size $width"));
    }
}
