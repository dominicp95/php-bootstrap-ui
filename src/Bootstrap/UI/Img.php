<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Elements\SingleElement;

class Img extends SingleElement {
    
    public function __construct(
        $src,
        $alt=null,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct('', $class, $style, $attributes);
        
        if( !is_null($alt) ){
            $this->addAttribute("alt", $alt);
            $this->addAttribute("title", $alt);
        }
        $this->setTag('img');
        $this->addAttribute("src", $src);
        $this->selfClose(true);
    }
}
