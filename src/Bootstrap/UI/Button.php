<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Elements\SingleElement;

class Button extends SingleElement{
    const BUTTON = "button";
    const SUBMIT = "submit";
    
    const BTN = "btn";
    
    const DEFAULT_STYLE = "btn btn-default";
    const PRIMARY = "btn btn-primary";
    const SUCCESS = "btn btn-success";
    const DANGER = "btn btn-danger";
    const WARNING = "btn btn-warning";
    const INFO = "btn btn-info";
    const LINK = "btn btn-link";
    
    const LARGE = "btn-lg";
    const SMALL = "btn-sm";
    const XSMALL = "btn-xs";
    const XLARGE = "btn-xl";
    
    const CIRCLE = "btn-circle";
    const BLOCK = "btn-block";
    
    public function __construct(
        $type,
        $html,
        $class = [],
        $style = [],
        $attributes = []
    ){
        parent::__construct($html, $class, $style, $attributes);
        
        $this->setTag('button');
        $this->addAttribute('type', $type);
    }
}
