<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\Dropdown;
use Bootstrap\UI\ListItem;

class DropdownItem extends ListItem{
   
    public function __construct(
        $html,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct($html, $class, $style, $attributes);
    }
}
