<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Elements\ElementGroup;

class PageHeader extends ElementGroup{
    
    public function __construct(
        $text,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        $children = [ new Heading( '1', $text , [ 'page-header' ] ) ];
        parent::__construct($children, $class, $style, $attributes);

        $this->setTag('div');
    }
    
    
    
}