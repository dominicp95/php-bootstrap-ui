<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Div;

class Panel extends Div {
    
    public function __construct(
        $type,
        $title,
        $body,
        $footer,
        $heading = \Bootstrap\UI\Heading::HEADING_1,
        $class = [],
        $style = [],
        $attributes = []
    ){
        $children = [
            new \Bootstrap\Elements\LogicElement(function(){
                if(func_get_args()[0] !== null){
                    $t = new \Bootstrap\Div( [ new \Bootstrap\UI\Heading( func_get_args()[3], func_get_args()[0] , ['panel-title'] ) ] , ['panel-heading'] );
                    $t->render();
                }
                if(func_get_args()[1] !== null){
                    $b = new \Bootstrap\Div( [ new \Bootstrap\UI\Text( func_get_args()[1]) ] , ['panel-body'] );
                    $b->render();
                }
                if(func_get_args()[2] !== null){
                    $f = new \Bootstrap\Div( [ new \Bootstrap\UI\Text( func_get_args()[2]) ] , ['panel-footer'] );
                    $f->render();
                }
            }, [ $title, $body, $footer, $heading ])
        ];
        parent::__construct($children, $class, $style, $attributes);
        
        $this->setTag('div');
        $this->appendAttribute('class', "panel");
        $this->appendAttribute('class', $type);
        
    }
    
    const PANEL_DEFAULT = "panel-default";
    const PANEL_PRIMARY = "panel-primary";
    const PANEL_SUCCESS = "panel-success";
    const PANEL_WARNING = "panel-warning";
    const PANEL_DANGER = "panel-danger";
    const PANEL_INFO = "panel-info";
    
    const XS1 = "col-xs-1";
    const XS2 = "col-xs-2";
    const XS3 = "col-xs-3";
    const XS4 = "col-xs-4";
    const XS5 = "col-xs-5";
    const XS6 = "col-xs-6";
    const XS7 = "col-xs-7";
    const XS8 = "col-xs-8";
    const XS9 = "col-xs-9";
    const XS10 = "col-xs-10";
    const XS11 = "col-xs-11";
    const XS12 = "col-xs-12";
    
    const SM1 = "col-sm-1";
    const SM2 = "col-sm-2";
    const SM3 = "col-sm-3";
    const SM4 = "col-sm-4";
    const SM5 = "col-sm-5";
    const SM6 = "col-sm-6";
    const SM7 = "col-sm-7";
    const SM8 = "col-sm-8";
    const SM9 = "col-sm-9";
    const SM10 = "col-sm-10";
    const SM11 = "col-sm-11";
    const SM12 = "col-sm-12";
    
    const MD1 = "col-md-1";
    const MD2 = "col-md-2";
    const MD3 = "col-md-3";
    const MD4 = "col-md-4";
    const MD5 = "col-md-5";
    const MD6 = "col-md-6";
    const MD7 = "col-md-7";
    const MD8 = "col-md-8";
    const MD9 = "col-md-9";
    const MD10 = "col-md-10";
    const MD11 = "col-md-11";
    const MD12 = "col-md-12";
    
    
    const LG1 = "col-lg-1";
    const LG2 = "col-lg-2";
    const LG3 = "col-lg-3";
    const LG4 = "col-lg-4";
    const LG5 = "col-lg-5";
    const LG6 = "col-lg-6";
    const LG7 = "col-lg-7";
    const LG8 = "col-lg-8";
    const LG9 = "col-lg-9";
    const LG10 = "col-lg-10";
    const LG11 = "col-lg-11";
    const LG12 = "col-lg-12";
}
