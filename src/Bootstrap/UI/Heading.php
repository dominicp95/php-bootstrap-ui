<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Elements\SingleElement;

class Heading extends SingleElement{
    
    const HEADING_1 = "1";
    const HEADING_2 = "2";
    const HEADING_3 = "3";
    const HEADING_4 = "4";
    const HEADING_5 = "5";
   
    public function __construct(
        $type,
        $html,
        $class=array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct($html, $class, $style, $attributes);
        
        $this->setTag('H'.$type);
    }
}
