<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;

class Video extends \Bootstrap\Elements\ElementGroup {
    
    public function __construct(
        $source,
        $type='video/mp4',
        $autoplay=false,
        $loop=false,
        $sound=false,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        $html = new \Bootstrap\Elements\SingleElement('');
        $html->setTag('source');
        $html->addAttribute("src", $source);
        $html->addAttribute("type", $type);      
        $html->selfClose(true);
                       
        parent::__construct( [$html] , $class, $style, $attributes);
        if($autoplay === true){
            $this->addAttribute("autoplay", true);
        }
        if($loop === true){
            $this->addAttribute("loop", true);
        }
        if($sound === false){
            $this->addAttribute("muted", true);
        }
        
        $this->setTag('video');
    }
}
