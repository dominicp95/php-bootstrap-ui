<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Span;

class Caret extends Span {
    
    public function __construct(
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct('', $class, $style, $attributes);
        
        $this->appendAttribute('class', 'caret');
        $this->setTag('span');
    }
}
