<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Elements\SingleElement;

class Label extends SingleElement{
    const DEFAULT_STYLE = "label label-default";
    const PRIMARY = "label label-primary";
    const SUCCESS = "label label-success";
    const DANGER = "label label-danger";
    const WARNING = "label label-warning";
    const INFO = "label label-info";
    const LINK = "label label-link";
    
    const LARGE = "label-lg";
    const SMALL = "label-sm";
    const XSMALL = "label-xs";
    
    public function __construct(
        $html,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct($html, $class, $style, $attributes);
        
        $this->setTag('span');
    }
}
