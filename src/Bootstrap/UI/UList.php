<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Elements\ElementGroup;

class UList extends ElementGroup{
    
    public function __construct(
        $children=array(),
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct($children, $class, $style, $attributes);
        $this->setTag('ul');
    }
    
    
    
}
class_alias('Bootstrap\UI\UList', 'Bootstrap\UI\NavGroup');