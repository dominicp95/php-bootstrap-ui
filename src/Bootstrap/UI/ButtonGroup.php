<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Div;

class ButtonGroup extends Div{
    
    public function __construct(
        $children,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct($children, $class, $style, $attributes);

        $this->appendAttribute('class', 'btn-group');
    }
}
