<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI\Table;

class BodyColumn extends \Bootstrap\Span {
    
    public function __construct(
        $html,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        parent::__construct($html, $class, $style, $attributes);
        
        $this->setTag('td');
    }
    
    
    const EXTRASMALL_1 = "col-xs-1";
    const EXTRASMALL_2 = "col-xs-2";
    const EXTRASMALL_3 = "col-xs-3";
    const EXTRASMALL_4 = "col-xs-4";
    const EXTRASMALL_5 = "col-xs-5";
    const EXTRASMALL_6 = "col-xs-6";
    const EXTRASMALL_7 = "col-xs-7";
    const EXTRASMALL_8 = "col-xs-8";
    const EXTRASMALL_9 = "col-xs-9";
    const EXTRASMALL_10 = "col-xs-10";
    const EXTRASMALL_11 = "col-xs-11";
    const EXTRASMALL_12 = "col-xs-12";
    
    const SMALL_1 = "col-sm-1";
    const SMALL_2 = "col-sm-2";
    const SMALL_3 = "col-sm-3";
    const SMALL_4 = "col-sm-4";
    const SMALL_5 = "col-sm-5";
    const SMALL_6 = "col-sm-6";
    const SMALL_7 = "col-sm-7";
    const SMALL_8 = "col-sm-8";
    const SMALL_9 = "col-sm-9";
    const SMALL_10 = "col-sm-10";
    const SMALL_11 = "col-sm-11";
    const SMALL_12 = "col-sm-12";
    
    const MEDIUM_1 = "col-md-1";
    const MEDIUM_2 = "col-md-2";
    const MEDIUM_3 = "col-md-3";
    const MEDIUM_4 = "col-md-4";
    const MEDIUM_5 = "col-md-5";
    const MEDIUM_6 = "col-md-6";
    const MEDIUM_7 = "col-md-7";
    const MEDIUM_8 = "col-md-8";
    const MEDIUM_9 = "col-md-9";
    const MEDIUM_10 = "col-md-10";
    const MEDIUM_11 = "col-md-11";
    const MEDIUM_12 = "col-md-12";
    
    
    const LARGE_1 = "col-lg-1";
    const LARGE_2 = "col-lg-2";
    const LARGE_3 = "col-lg-3";
    const LARGE_4 = "col-lg-4";
    const LARGE_5 = "col-lg-5";
    const LARGE_6 = "col-lg-6";
    const LARGE_7 = "col-lg-7";
    const LARGE_8 = "col-lg-8";
    const LARGE_9 = "col-lg-9";
    const LARGE_10 = "col-lg-10";
    const LARGE_11 = "col-lg-11";
    const LARGE_12 = "col-lg-12";
}
