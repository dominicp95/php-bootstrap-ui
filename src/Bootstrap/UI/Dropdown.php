<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\UI\UList;
use Bootstrap\UI\Caret;
use Bootstrap\UI\A;
use Bootstrap\Elements\ElementGroup;

class Dropdown extends ElementGroup{
    
    public function __construct(
        $label,
        $children=array(),
        $class = array('dropdown'),
        $style=array(),
        $attributes=array()
    ){
        $c = array(
            new A(
                '#',
                trim($label.' '.new Caret()), 
                [ 'dropdown-toggle' ], 
                [], 
                [ 'data-toggle'=>"dropdown", 'role'=>"button", 'aria-haspopup'=>"true", 'aria-expanded'=>"false" ]
            ),
            new UList($children, [ 'dropdown-menu' ]),
        );
        parent::__construct($c, $class, $style, $attributes);
        $this->setTag('li');
    }
    
    
    
}