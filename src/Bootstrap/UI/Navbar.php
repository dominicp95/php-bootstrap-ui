<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Elements\ElementGroup;
use Bootstrap\Container;
use Bootstrap\Div;
use Bootstrap\UI\Button;
use Bootstrap\UI\A;
use Bootstrap\UI\UList;

class Navbar extends ElementGroup{
    const FLUID = "container-fluid";
    const FIXED = "container";
    
    public function __construct(
        $type,
        $brand,
        $children=array(),
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
        $c = [
            new Container( $type, 
                [
                    new Div( 
                            [ 
                                new Button('button', (new \Bootstrap\Span('', ['icon-bar'])).(new \Bootstrap\Span('', ['icon-bar'])).(new \Bootstrap\Span('', ['icon-bar'])),
                                    [ 'navbar-toggle', 'collapsed'],
                                    [],
                                    [ 'data-toggle'=>"collapse", 'data-target'=>"#navbar", 'aria-expanded'=>"false", 'aria-controls'=>"navbar" ]
                                ),
                                new A($brand['link'], $brand['title'] , ['navbar-brand'])
                            ],
                            ['navbar-header']
                    ),
                    new Div( 
                        [
                            new UList(
                                    $children,
                                    [ 'nav', 'navbar-nav' ]
                            )
                        ], ['navbar-collapse', 'collapse'], [], ['id'=>"navbar"]
                    )
                ]
            )
        ];
        
        parent::__construct($c, $class, $style, $attributes);
        
        $this->appendAttribute('class', 'navbar');
        $this->setTag('nav');
    }
    
    
    
}