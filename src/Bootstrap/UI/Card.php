<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;

class Card extends \Bootstrap\Elements\EmptyElement {
    
    public function __construct(
        $type,
        $icon,
        $desc,
        $number,
        $link="#",
        $class=[],
        $style=[],
        $attributes=[]
    ){
        $panel = (new \Bootstrap\UI\Panel( $type ,
            (new \Bootstrap\UI\Row([ 
                new \Bootstrap\UI\Col( \Bootstrap\UI\Col::EXTRASMALL_3, [ new \Bootstrap\UI\FAIcon($icon, \Bootstrap\UI\FAIcon::SIZE_4X) ]),
                new \Bootstrap\UI\Col( \Bootstrap\UI\Col::EXTRASMALL_9, [ 
                    new \Bootstrap\Div([ new \Bootstrap\UI\Text($number) ], ['huge']),
                    new \Bootstrap\Div([ new \Bootstrap\UI\Text($desc) ])
                ], ['text-right'])
            ])),
            null,
            new \Bootstrap\UI\A( $link, (new \Bootstrap\Elements\ElementGroup([ 
                new \Bootstrap\Span( "View Details", ['pull-left'] ),
                new \Bootstrap\Span( new \Bootstrap\UI\FAIcon( 'arrow-circle-right' ), ['pull-right'] ),
                new \Bootstrap\Div( [], ['clearfix'] )
            ])) ),
            \Bootstrap\UI\Heading::HEADING_1,
            $class,
            $style,
            $attributes
        ));
        parent::__construct($panel, $class, $style, $attributes);
                
    }
    
    const CARD_DEFAULT = "panel-default";
    const CARD_PRIMARY = "panel-primary";
    const CARD_SUCCESS = "panel-success";
    const CARD_WARNING = "panel-warning";
    const CARD_DANGER = "panel-danger";
}
