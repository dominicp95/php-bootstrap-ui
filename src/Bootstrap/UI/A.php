<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author gb107439
 */
namespace Bootstrap\UI;
use Bootstrap\Elements\SingleElement;

class A extends SingleElement{
    
    public function __construct(
        $href,
        $text,
        $class = array(),
        $style=array(),
        $attributes=array()
    ){
         parent::__construct($text, $class, $style, $attributes);
        
        $this->setTag('a');
        $this->addAttribute("href", $href);
    }
}
